#!/usr/bin/env python
# -*- coding: utf8 -*-
import torch
import os
from overfit_190326113036.model import UNet

model_path = "./worthly_sweep_49.ptn"
model = UNet(1, 4).cuda()
model.load_state_dict(torch.load(model_path))

def can_binary():
    return False

def can_multiclass():
    return True

def can_confidence():
    return False

def segment_binary(raw, catheters):
    return False

def predict_confidence(raw, catheters):
    return False

def segment_multiclass(raw, catheters):
    out = model(raw)#Raw: Batchx1x512x600, values [0,255]
    max, max_indices = torch.max(out, dim=1)
    #output: multiclass_mask: Batchx1x512x600 [0, 1, 2, 3, 4]
    #0: Red
    #1: Blue
    #2: Yellow
    #3: Green (this model does not have green)
    #4: Black

    # but my training was # 0=RG, 1=Y, 2=B, 3=Black
    # so at first plus 1, # 1=RG, 2=Y, 3=B, 4=Black
    output = max_indices + 1

    # and change r=1 to 0, b=3 to 1 # 0=RG, 2=Y, 1=B, 4=Black
    r = 1 * (output==1)
    output = output - r

    b = 2 * (output == 3)
    output = output - b

    output = torch.reshape(output, (output.shape[0],1,output.shape[1],output.shape[2]))
    return output


