
import torch

class UNet(torch.nn.Module):

    def __init__(self, n_channels, n_classes):
        from . import unet_parts
        super(UNet, self).__init__()
        start_channel = 32

        self.inc = unet_parts.inconv(n_channels, start_channel)
        self.down1 = unet_parts.down(start_channel, start_channel*2)
        self.down2 = unet_parts.down(start_channel*2, start_channel*4)
        self.down3 = unet_parts.down(start_channel*4, start_channel*8)
        self.down4 = unet_parts.down(start_channel*8, start_channel*8)
        self.up1 = unet_parts.up(start_channel*16, start_channel*4)
        self.up2 = unet_parts.up(start_channel*8, start_channel*2)
        self.up3 = unet_parts.up(start_channel*4, start_channel)
        self.up4 = unet_parts.up(start_channel*2, start_channel)
        self.outc = unet_parts.outconv(start_channel, n_classes)
        print("succeed to open")

    def forward(self, x):
        x1 = self.inc(x)
        # print("x1:"+str(x1.shape))
        x2 = self.down1(x1)
        # print("x2:"+str(x2.shape))
        x3 = self.down2(x2)
        # print("x3:"+str(x3.shape))
        x4 = self.down3(x3)
        # print("x4:"+str(x4.shape))
        x5 = self.down4(x4)
        # print("x5:"+str(x5.shape))

        x = self.up1(x5, x4)
        # print("up1:"+str(x.shape))
        x = self.up2(x, x3)
        # print("up2:"+str(x.shape))
        x = self.up3(x, x2)
        # print("up3:"+str(x.shape))
        x_before = self.up4(x, x1)
        # print("up4:"+str(x.shape))
        x = self.outc(x_before)
        return x#, x_before
