import torch
from overfit_190326113036.segmentation import segment
from torchvision import models, transforms
from PIL import Image

img_path = r"/nas_share/10_GroundTruthMaking/04_Task/Chunk_190326113036/190326113036/rt/0.png"
raw_trans = transforms.Compose([transforms.Grayscale(), transforms.ToTensor()])
raw_img = Image.open(img_path)
raw_img = raw_trans(raw_img).cuda()
raw_img = torch.reshape(raw_img,(1,1,512,600))


output = segment(raw_img)
print(output.shape)

for i in range(0,511):
    print(output[0,0,i,350])
# change class order
# change color

# save image
